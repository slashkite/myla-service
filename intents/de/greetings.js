const random = array => { return array[Math.floor(Math.random() * array.length)] }

const getGreetings = () => {
  const answers = [
    'Hallo!',
    'Jo.',
    'Grüß Gott.',
    'Servus!',
    'Hi, wie kann ich dir helfen?',
    'Hey, was brauchst du?',
  ]
  return random(answers)
}

module.exports = getGreetings