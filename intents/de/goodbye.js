const random = array => { return array[Math.floor(Math.random() * array.length)] }

const getGoodbyes = () => {
  const answers = [
    'Ciao!',
    'Bis bald.',
    'Wir sehen uns.',
    'Servus!'
  ]
  return random(answers)
}

module.exports = getGoodbyes