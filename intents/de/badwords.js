﻿const random = array => { return array[Math.floor(Math.random() * array.length)] }

const getBadwords = () => {
  const answers = [
    'Selber',
    'Sei nicht so frech.',
    'Du kannst mich mal.',
    'Schau in den Spiegel.',
    'Wow, das lässt mich kalt.',
    'Fällt dir nichts besseres ein?'
  ]
  return random(answers)
}

module.exports = getBadwords