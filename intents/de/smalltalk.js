﻿const random = array => { return array[Math.floor(Math.random() * array.length)] }

const getSmallTalks = () => {
  const answers = [
    'Nichts besonderes. Und du?',
    'Kann man so sagen.',
    'Wirklich?',
    'Und dann?',
    'Wie ist das passiert?'
  ]
  return random(answers)
}

module.exports = getSmallTalks