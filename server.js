﻿const recastai = require('recastai');
const client = new recastai.request('8359952b2bb7f5dca01b56e88f418da9', 'de');
const express = require('express');
const bodyParser = require('body-parser');
require('log-timestamp');

// intents
const INTENTS = {
    DE: {
        greetings: require('./intents/de/greetings.js'),
        goodbye: require('./intents/de/goodbye.js'),
        smalltalk: require('./intents/de/smalltalk.js'),
        badwords: require('./intents/de/badwords.js')
    }
}


var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/myla', function (req, res) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log('POST /myla [IP:' + ip + ']');
    if (!req.body || typeof req.body.message === "undefined") {
        res.status(200).send({ 'action': 'error', 'speak': 'Ein Fehler ist aufgetreten.' });
        return;
    }

    var text = req.body.message;
    text = text.toLowerCase();
    var language = req.body.language;

    console.log('Text=%s Language=%s', text, language);

    var action = { 'action': 'speak', 'speak': 'Verstehe ich nicht.' };
    client.converseText(text)
        .then(aires => {

            if (aires.intents.length > 0) {
                const intent = aires.intents[0].slug;
                console.log('Intent=%s', intent);
    
                if (intent) {
                    action = { 'action': 'speak', 'speak': INTENTS[language][intent]() };
                }
            } else {
                action = { 'action': 'speak', 'speak': 'Das kenne ich noch nicht.' };
            }

            console.log('Action=%s', JSON.stringify(action));
            res.status(200).send(action);
        })
        .catch((err) => {
            console.log('Error=%s', err);
            res.status(200).send(action);
        });
});

app.listen(16003, () => console.log('MYLA Service listening on port 16003'));

